#include <iostream>
using namespace std;
class Animal
{
  public:
    void sound();
    void action() //Action of an animal
    {
        cout << " shouting " << endl;
    }
};
class Dog : public Animal
{
  public:
    void sound() //sound of a dog
    {
      cout << "Bow - Bow " << endl;
    }
};
class Cat : public Animal
{
  public:
    void sound() // sound of a cat
    {
      cout << "Meow - Meow " << endl;
    }
};

int main()
{
  Dog d; //creating object for Dog class
  Cat c; //creating object for Cat class
  d.action();
  d.sound();
  c.action();
  c.sound();
  return 0;
